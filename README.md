### lazy man python script
```
python docker.py -w fe/be/both -c build/up/down 
```
### build and run docker-compose container
```
docker-compose -f docker-compose.ENV.development.yml up --build -d
```
### run docker-compose container
```
docker-compose -f docker-compose.ENV.development.yml up
```
### stop docker-compose container
```
docker-compose -f docker-compose.ENV.development.yml down
```
Where ENV: fe (frontend) or be (backend)

### without docker-compose
```
cd frontend
docker build fe_nuxt .
docker run -d -p 3000:80 fe_nuxt
cd ../backend
docker build be_fastapi .
docker run -d -p 8000:80 be_fastapi
```
### remove image
```
docker image rm <IMAGE_NAME>
```
### stop container
```
docker container stop <NAME/ID>
```
