from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import json
from os import path

app = FastAPI()

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/predictions")
def get_predictions():
    try:
        with open(path.join('..', 'mock/predictions.json')) as f:
            data = json.load(f)
    except:
        return {'status': -1}
    return data
