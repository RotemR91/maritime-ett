import argparse, subprocess, os
parser = argparse.ArgumentParser(
    description=(
        ''
    ),
    formatter_class=argparse.RawDescriptionHelpFormatter
)

parser.add_argument(
    '-w', '--which',
    help='which docker-compose to spin up',
    choices=['fe', 'be', 'both'],
    type=str,
    default='both'
)

parser.add_argument(
    '-p', '--production',
    help='mode to spin docker up in',
    action="store_true",
    default=False
)

parser.add_argument(
    '-c', '--command',
    help='what to do with docker-compose',
    choices=['build', 'up', 'down'],
    type=str,
    default='up'
)

parser.add_argument(
    '-nc', '--no-cache',
    help='no cache',
    action="store_true",
    default=False
)

args = parser.parse_args()

os.environ['DOCKER_BUILDKIT'] = '1'
os.environ['COMPOSE_DOCKER_CLI_BUILD'] = '1'

command = [
    'docker-compose'
]

CHOICE_MAP = {
    'fe': 'fe',
    'be': 'be',
}

DOCKER_COMPOSE_FILE_TEMPLATE = 'docker-compose.MODE.ENV.yml'
PRODUCTION_TEMPLATE = DOCKER_COMPOSE_FILE_TEMPLATE.replace('ENV', 'production')
DEVELOPMENT_TEMPLATE = DOCKER_COMPOSE_FILE_TEMPLATE.replace('ENV', 'development')

is_production = args.production
if args.which == 'both':
    mode = 'both'
else:
    mode = CHOICE_MAP[args.which]


def stack_compose_files(command_list, mode, is_production):
    if mode in ['fe', 'be']:
        # command_list.append('-f')
        # command_list.append(PRODUCTION_TEMPLATE.replace('MODE', mode))
        if is_production == False:
            command_list.append('-f')
            command_list.append(DEVELOPMENT_TEMPLATE.replace('MODE', mode))
    return command_list

if mode == 'both':
    for m in ['fe', 'be']:
        command = stack_compose_files(command, m, is_production)
else:
    command = stack_compose_files(command, mode, is_production)



command.append(args.command)

if args.no_cache:
    command.append('--no-cache')

subprocess.run(command)
