import axios from 'axios'

const isDev = false

const baseURLBusinessLogic = 'http://localhost:8000'
export const apiBL = axios.create({
  baseURL: baseURLBusinessLogic
})
export const apiMock = axios

export const api =  isDev ? axios : apiBL


export const getPredictions = async ({
  data
}) => {
  const axiosGetConfig = {
    params:{
      data: data,
    }
  }
  try {
    const uri = isDev ? `mock.json` : '/predictions'
    const response = await api.get(uri, axiosGetConfig)
    if (response.status !== 200) throw 'Status not 200!'
    return response.data
  } catch (error) {
    // TODO: add proper error handling
    console.error(error)
    return undefined
  }
}
