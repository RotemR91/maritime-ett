import {
  getPredictions,
} from '@/api/api.js'

const actions = {
  async fetchPredictions({state, commit, dispatch}, {data}) {
    let resp = await getPredictions({data})
    if(resp) {
      commit('setPredictions', {data: resp})
    } else {
      dispatch('updateGlobalSnack', {
        text: 'Error fetching predictions',
        show: true,
        type: 'error'
      })
    }
  },
  updateGlobalSnack({state, commit}, {text, type, show}) {
    commit('setGlobalSnack', {text, type, show})
  }
}

export default actions
