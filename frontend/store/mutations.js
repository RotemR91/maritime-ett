import Vue from 'vue'

const mutations = {
  setPredictions(state, {data}) {
    let key = 'predictions'
    if (data === null) {
      state[key] = null
      return
    }
    Vue.set(state, key, data)
  },
  setGlobalSnack(state, {text, type='info', show}) {
    if (type !== undefined) {
      state.globalSnackType = type
    }
    state.globalSnackMessaage = text
    if (typeof show === 'boolean') {
      state.globalSnackModel = show
    }
  },
}

export default mutations
