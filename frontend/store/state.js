const state = () => ({
  predictions: null,
  globalSnackModel: false,
  globalSnackType: '',
  globalSnackMessaage: ''
})

export default state
